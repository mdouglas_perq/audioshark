﻿/**
 * Audioshark Servent, v0.1
 * File Server Component
 * Copyright (c) 2009 Matthew Douglas, Jeff Sorbo
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 **/

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Audioshark.Servent
{
    /// <summary>
    /// Handles a single HTTP request.
    /// </summary>
    public class FileRequestHandler
    {
        // Implemented HTTP status phrases
        private const string STATUS_OK = "200 OK",
                             STATUS_BADREQUEST = "400 Bad Request",
                             STATUS_FORBIDDEN = "403 Forbidden",
                             STATUS_NOTFOUND = "404 Not Found",
                             STATUS_SERVERERROR = "500 Internal Server Error",
                             STATUS_NOTIMPLEMENTED = "501 Not Implemented",
                             STATUS_NOTSUPPORTED = "505 HTTP Version Not Supported";

        // The server that received the request
        private FileServer server;
        

        // The requesting client
        private Socket client;

        // HTTP Version
        private const string HTTP_VERSION = "HTTP/1.1";

        // Response headers
        Dictionary<string, string> headers = new Dictionary<string, string>();

        bool headerSent = false;

        /// <summary>
        /// Creates a new FileRequestHandler.
        /// </summary>
        /// <param name="svr">The server that received the request.</param>
        /// <param name="c">The client that sent the request.</param>
        public FileRequestHandler(FileServer svr, Socket c)
        {
            this.server = svr;
            this.client = c;

            // Set up some default response headers
            headers["Server"] = this.server.Version;
            headers["Connection"] = "close";
            headers["Content-Type"] = "text/html";
        }

        /// <summary>
        /// Sends a response body.
        /// </summary>
        /// <param name="message">A message to show on the page.</param>
        /// <remarks>This is intended to be used for debugging error pages.</remarks>
        private void SendBody(string message)
        {
            if (headerSent)
            {
                string body = message + "<hr /><em>" + this.server.Version + "</em>";
                client.Send(Encoding.ASCII.GetBytes(body));
            }
        }

        /// <summary>
        /// Sends the HTTP Response headers.
        /// </summary>
        /// <param name="status">The HTTP status code and phrase to report.</param>
        private void SendHeader(string status)
        {
            if (!headerSent)
            {
                // Prepare HTTP Response
                string data = HTTP_VERSION + " " + status + "\r\n";

                Console.WriteLine("FileRequestHandler: Sending response code '{0}' to {1}", 
                                    status, this.client.RemoteEndPoint.ToString());

                // Add headers
                foreach (KeyValuePair<string, string> kvp in headers)
                    data += kvp.Key + ": " + kvp.Value + "\r\n";
                data += "\r\n"; 

                // Send the response headers
                client.Send(Encoding.ASCII.GetBytes(data));

                headerSent = true;
            }
        }

        /// <summary>
        /// Processes an HTTP Request.
        /// </summary>
        public void HandleRequest()
        {
            // Request buffer
            byte[] reqBuffer = new byte[1024];  // I know this is lame, but requests should be smaller than this.
            client.Receive(reqBuffer);
            string request = Encoding.ASCII.GetString(reqBuffer);

            // Naive parsing of HTTP request
            if (request.IndexOf(HTTP_VERSION) < 0)
            {
                SendHeader(STATUS_NOTSUPPORTED);
                SendBody("<h1>HTTP 505 Version Not Supported</h1>");
            }
            else if (request.StartsWith("GET"))
            {
                request = request.Split('\r')[0].Substring(4);   // We only care about the first line.
                request = request.Substring(0, request.IndexOf(HTTP_VERSION) - 1);

                // Should now have the request URI. TODO: Decode this for %20 etc.

                request = request.Replace("..", "");
                request = request.Replace('/', Path.DirectorySeparatorChar);
                request = request.Trim();

                Console.WriteLine("FileRequestHandler: Received request for '{0}'", request);

                // Full path of the requested file.
                string filePath = this.server.ShareDirectory + request;

                // Check for existence of requested file
                if (File.Exists(filePath))
                {
                    // Make sure this is an MP3 file
                    if (!Path.GetExtension(filePath).Equals(".mp3", StringComparison.InvariantCultureIgnoreCase))
                    {
                        SendHeader(STATUS_FORBIDDEN);
                        SendBody("<h1>HTTP 403 Forbidden</h1><p>You do not have permission to access this resource.</p>");
                    }
                    else
                    {
                        FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                        NetworkStream ns = new NetworkStream(client);

                        // Send File
                        try
                        {
                            long length = fs.Length;    // File length
                            byte[] buffer = new byte[4096]; // Read buffer

                            long bytesRead = 0;
                            long bytesRemaining = length;

                            headers["Content-Length"] = length.ToString();
                            headers["Content-Type"] = "octet/stream";
                            headers["Content-Disposition"] = "attachment; filename=" + Path.GetFileName(filePath);

                            SendHeader(STATUS_OK);

                            // Create unique key for this upload
                            string key = Guid.NewGuid().ToString();

                            // Dispatch upload creation event
                            this.server.UploadBegin(key, Path.GetFileName(filePath), length.ToString(), "<NotImplemented>",
                                        ((IPEndPoint)client.RemoteEndPoint).Address.ToString());

                            Console.WriteLine("FileRequestHandler: Beginning upload of '{0}'", Path.GetFileName(filePath));

                            // Read from the file, send it in 0-4KB pieces
                            while (bytesRemaining > 0)
                            {
                                // will read anywhere from 0 to buffer.Length bytes
                                int n = fs.Read(buffer, 0, buffer.Length);

                                if (n == 0)     // EOF (Error?)
                                    break;

                                // send data
                                ns.Write(buffer, 0, buffer.Length);

                                this.server.UploadProgress(key, bytesRead, length);

                                bytesRead += n;
                                bytesRemaining -= n;

                                // clear buffer
                                buffer.Initialize();
                            }

                            this.server.UploadComplete(key);

                            Console.WriteLine("FileRequestHandler: Completed upload of '{0}'", Path.GetFileName(filePath));

                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("FileRequestHandler: Error: {0}", ex.Message);
                            SendHeader(STATUS_SERVERERROR);
                            SendBody("<h1>HTTP 500 Internal Server Error</h1> " +
                                    "<pre>" + ex.Message + "\r\n\r\n" + ex.StackTrace + "</pre>");
                        }
                        finally
                        {
                            fs.Close();
                            ns.Close();
                        }
                    }
                }
                else // File not found!
                {
                    SendHeader(STATUS_NOTFOUND);
                    SendBody("<h1>HTTP 404 Not Found</h1>");
                }
            }
            else if (request.StartsWith("HEAD") || request.StartsWith("POST") ||
                     request.StartsWith("PUT") || request.StartsWith("DELETE") ||
                     request.StartsWith("TRACE") || request.StartsWith("OPTIONS") ||
                     request.StartsWith("CONNECT"))
            {
                SendHeader(STATUS_NOTIMPLEMENTED);
                SendBody("<h1>HTTP 501 Not Implemented</h1>");
            }
            else
            {
                SendHeader(STATUS_BADREQUEST);
                SendBody("<h1>HTTP 400 Bad Request</h1>");
            }

            // Close the connection after 1 second timeout
            client.Close(1);

            // Force garbage collection
            GC.Collect();
        }
    }
}
