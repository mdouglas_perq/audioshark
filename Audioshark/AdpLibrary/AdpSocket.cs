﻿/**
 * Audioshark Directory Protocol Library, v0.1
 * Copyright (c) 2009 Matthew Douglas, Jeff Sorbo
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 **/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Timers;

namespace Audioshark.AdpLibrary
{
    /// <summary>
    /// Represents the possible states of the socket.
    /// </summary>
    public enum AdpSocketState
    {
        /// <summary>
        /// The socket is closed. Reading and Writing are not allowed.
        /// </summary>
        Closed,

        /// <summary>
        /// The socket is waiting for an incoming connection. Reading and Writing are not allowed.
        /// </summary>
        Listening,

        /// <summary>
        /// The socket is waiting for a connection acknowledgement.
        /// </summary>
        SynSent,

        /// <summary>
        /// The socket is waiting for a connection acknowledgement.
        /// </summary>
        SynReceived,

        /// <summary>
        /// The socket is connected. Reading and Writing are allowed.
        /// </summary>
        Established
    }

    /// <summary>
    /// 
    /// </summary>
    public class AdpSocket
    {
        /// <summary>
        /// The current state of the socket.
        /// </summary>
        private AdpSocketState state = AdpSocketState.Closed;

        /// <summary>
        /// Queue of packets waiting to be sent.
        /// </summary>
        private Queue<AdpPacket> sendQueue;

        /// <summary>
        /// The last ADP packet that was sent.
        /// </summary>
        private AdpPacket lastPacketSent;

        /// <summary>
        /// Buffer of received data fragments.
        /// </summary>
        private List<AdpPacket> recvBuffer;

        /// <summary>
        /// Underlying UDP socket
        /// </summary>
        private Socket udp;


        private Timer timeout;

        /// <summary>
        /// Constructor.
        /// </summary>
        public AdpSocket()
        {
            udp = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            

            // Do some ReceiveMessageFromAsync stuff
        }


        public void Connect(IPEndPoint ep)
        {
            // Create initial SYN packet
            AdpPacket syn = new AdpPacket(AdpPacketFlags.Syn);
            syn.SequenceNumber = 0x1337;      // This should later be random

            this.sendQueue.Enqueue(syn);

            // send SYN, update state = SYN_SENT

        }

        private void ProcessSendQueue()
        {
            // check if we're waiting for an ACK on last sent

            AdpPacket p = this.sendQueue.Dequeue();

            // call send(p.GetBytes())
            // reset send timeout timer
        }


        // Receive: ReceiveMessageFromAsync,
        // Create ADP Packet,
        // Process

        // Some Recieve Events:
        // ReciveRaw, ReceiveAdpPacket, ReceiveMessage

        // Some Send abilities:
        // SendPacket (private)
        // SendMessage
        // These push packets on to the send queue
        

        // packets from the Send Queue are sent after an ACK is received
        // when


        /// <summary>
        /// Gets the current state of the socket.
        /// </summary>
        public AdpSocketState State
        {
            get { return this.state; }
        }
    }
}
