﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AudiosharkDirectoryServer
{
    public partial class frmAudioSharkDirectoryServer : Form
    {
        private AudiosharkDirectoryServer server;

        public frmAudioSharkDirectoryServer()
        {
            InitializeComponent();
        }

        private void frmAudioSharkDirectoryServer_Load(object sender, EventArgs e)
        {
            server = new AudiosharkDirectoryServer();
            cmdStart.Enabled = true;
            cmdStop.Enabled = false;
        }

        private void cmdStart_Click(object sender, EventArgs e)
        {
            server.Start();
            cmdStart.Enabled = false;
            cmdStop.Enabled = true;
        }

        private void cmdStop_Click(object sender, EventArgs e)
        {
            server.Stop();
            cmdStart.Enabled = true;
            cmdStop.Enabled = false;
        }
    }
}
