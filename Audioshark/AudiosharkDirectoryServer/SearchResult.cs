﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace AudiosharkDirectoryServer
{
    class SearchResult
    {
        private string ipAddress;
        private string hostname;
        private DirectoryEntry entry;

        public string IPAddress
        {
            get { return ipAddress; }
            set { ipAddress = value; }
        }

        public string Hostname
        {
            get { return hostname; }
            set { hostname = value; }
        }

        public DirectoryEntry Entry
        {
            get { return entry; }
            set { entry = value; }
        }

        public SearchResult(string ipAddress, string hostname, DirectoryEntry entry)
        {
            this.ipAddress = ipAddress;
            this.hostname = hostname;
            this.entry = entry;
        }
    }
}
