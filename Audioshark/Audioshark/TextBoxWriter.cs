﻿using System;
using System.Text;
using System.Windows.Forms;

// Based on Article located here: 
// http://www.developer.com/net/net/article.php/3599261/Redirect-IO-to-a-TextBoxWriter-in-NET.htm

namespace Audioshark.Servent
{
    public class TextBoxWriter : System.IO.TextWriter
    {
        private TextBoxBase textBox;
        private StringBuilder sb;

        public TextBoxWriter(TextBox box)
        {
            this.textBox = box;
            box.HandleCreated += new EventHandler(OnHandleCreated);
        }
        public override void Write(char value)
        {
            Write(value.ToString());
        }

        public override void Write(string value)
        {
            if (this.textBox.IsHandleCreated)
                this.AppendText(value);
            else
                this.BufferText(value);
        }

        public override void WriteLine(string value)
        {
            this.Write(value + Environment.NewLine);
        }

        private void BufferText(string s)
        {
            if (this.sb == null)
                this.sb = new StringBuilder();
            this.sb.Append(s);
        }

        private void AppendText(string s)
        {
            if (this.sb != null)
            {
                this.SafeAppendText(sb.ToString());
                this.sb = null;
            }

            this.SafeAppendText(s);
        }

        private void SafeAppendText(string s)
        {
            if (this.textBox.InvokeRequired)
                this.textBox.BeginInvoke(new MethodInvoker(() => this.SafeAppendText(s)));
            else
                this.textBox.AppendText(s);
        }
        private void OnHandleCreated(object sender, EventArgs e)
        {
            if (this.sb != null)
            {
                this.SafeAppendText(this.sb.ToString());
                this.sb = null;
            }
        }

        public override Encoding Encoding
        {
            get { return Encoding.Default; }
        }
    }
}
