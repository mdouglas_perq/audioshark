﻿/**
 * Audioshark Servent, v0.1
 * File Server Component
 * Copyright (c) 2009 Matthew Douglas, Jeff Sorbo
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 **/

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Net.Sockets;

namespace Audioshark.Servent
{
    /// <summary>
    /// This class implements a basic HTTP file server.
    /// </summary>
    public class FileServer
    {
        // Delegates types for event handlers
        public delegate void FileServerEventHandler(string key);
        public delegate void FileServerUploadBeginEventHandler(string key, string filename, string size, string hostname, string ip);
        public delegate void FileServerProgressHandler(string key, long bytesRead, long bytesTotal);
        public delegate void FileServerErrorHandler(string key, string errorMessage);

        // Events
        public event FileServerUploadBeginEventHandler OnUploadBegin;
        public event FileServerEventHandler OnUploadComplete;
        public event FileServerProgressHandler OnProgressUpdate;
        public event FileServerErrorHandler OnError;

        private int port = 8490;
        private Socket listener;
        private bool running = false;
        private Thread listenThread;

        private string shareDir = "\\Shared";

        /// <summary>
        /// Returns the location of the shared directory.
        /// </summary>
        public string ShareDirectory
        {
            get { return this.shareDir; }
        }

        /// <summary>
        /// Returns a version string.
        /// </summary>
        public string Version
        {
            get 
            { 
                return System.Windows.Forms.Application.ProductName + "/" +
                         System.Windows.Forms.Application.ProductVersion + " (" +
                         Environment.OSVersion.VersionString + ")"; 
            }
            
        }

        public FileServer()
        {
            this.Initialize();
        }

        public FileServer(string sharedDirectory)
        {
            this.shareDir = sharedDirectory;

            this.Initialize();
        }

        public void Initialize()
        {
            if (!Directory.Exists(shareDir))
                Directory.CreateDirectory(shareDir);

            Console.WriteLine("FileServer: Initialized. Path='{0}'", Path.GetFullPath(shareDir));
        }

        /// <summary>
        /// Starts the server.
        /// </summary>
        public void Start()
        {
            if (running) return;

            listenThread = new Thread(new ThreadStart(this.Listen));
            listenThread.Start();
            running = true;
        }

        /// <summary>
        /// Listens continuously for requests.
        /// </summary>
        /// <remarks>This is a blocking method. It runs in its own background thread.</remarks>
        private void Listen()
        {
            try
            {
                // Create a listening TCP socket
                listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                // Bind to local address and port
                System.Net.EndPoint localEP = new System.Net.IPEndPoint(System.Net.IPAddress.Any, this.port);
                listener.Bind(localEP);

                // Start listening for connections
                listener.Listen(1024);
                Console.WriteLine("FileServer: Listening on {0}", listener.LocalEndPoint.ToString());

                // Connecting client socket
                Socket client;

                // Accept connections
                while (this.running)
                {
                    // Accept a connection
                    client = listener.Accept();
                    Console.WriteLine("FileServer: Received connection from {0}", client.RemoteEndPoint.ToString());

                    // Create a FileRequestHandler object to handle the request
                    FileRequestHandler req = new FileRequestHandler(this, client);

                    // Start a new thread for the HTTP session
                    Thread t = new Thread(new ThreadStart(req.HandleRequest));
                    t.Start();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("FileServer.Listen Exception: {0}", ex.Message);
            }
        }

        /// <summary>
        /// Stops the server.
        /// </summary>
        public void Stop()
        {
            if (!running) return;
            running = false;
        }

        #region Event Proxies
        public void UploadBegin(string key, string filename, string size, string hostname, string ip)
        {
            if (this.OnUploadBegin != null)
                this.OnUploadBegin(key, filename, size, hostname, ip);
        }

        public void UploadComplete(string key)
        {
            if (this.OnUploadComplete != null)
                this.OnUploadComplete(key);
        }

        public void UploadProgress(string key, long bytesRead, long bytesTotal)
        {
            if (this.OnProgressUpdate != null)
                this.OnProgressUpdate(key, bytesRead, bytesTotal);
        }

        public void UploadError(string key, string errorMessage)
        {
            if (this.OnError != null)
                this.OnError(key, errorMessage);
        }
        #endregion

    }
}
