﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AudiosharkDirectoryServer
{
    class DirectoryEntry
    {
        private string filename;
        private long size;

        public string Filename
        {
            get { return filename; }
            set { filename = value; }
        }

        public long Size
        {
            get { return size; }
            set { size = value; }
        }

        public DirectoryEntry(string filename, long size)
        {
            this.filename = filename;
            this.size = size;
        }

        public DirectoryEntry() : this("", 0) { }
    }
}
