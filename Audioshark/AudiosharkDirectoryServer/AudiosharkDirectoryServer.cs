﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace AudiosharkDirectoryServer
{
    class AudiosharkDirectoryServer
    {
        private const int BACKLOG = 1024;
        private const int LISTENER_PORT = 4490;

        private Dictionary<Guid,MemberHandler> memberHandlers;
        private Socket listener;
        private Thread listenerThread;
        private bool running = false;

        public void Start()
        {
            if (running) return;

            memberHandlers = new Dictionary<Guid,MemberHandler>();

            listenerThread = new Thread(new ThreadStart(Listen));
            listenerThread.Start();
            running = true;
        }

        public void Stop()
        {
            if (!running) return;
            running = false;
        }

        public void Listen()
        {
            try
            {
                // Create a listening TCP socket
                listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                // Bind to local address and port
                EndPoint localEP = new IPEndPoint(IPAddress.Any, LISTENER_PORT);
                listener.Bind(localEP);

                // Start listening for memberHandlers
                listener.Listen(1024);
                Console.WriteLine("Listening on port " + LISTENER_PORT.ToString());

                // Connecting client socket
                Socket client;

                // Accept memberHandlers
                while (this.running)
                {
                    Console.WriteLine("Waiting for connection...");

                    // Accept a connection
                    client = listener.Accept();
                    Console.WriteLine("Received connection from " + client.RemoteEndPoint.ToString());

                    // Create a MemberHandler object to handle the request
                    Guid newGuid = Guid.NewGuid();
                    MemberHandler handler = new MemberHandler(newGuid, this, client);

                    // Add this MemberHandler to the list
                    memberHandlers.Add(newGuid, handler);

                    // Start a new thread for the HTTP session
                    Thread t = new Thread(new ThreadStart(handler.HandleRequests));
                    t.Start();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("AudiosharkDirectoryServer::Listen Exception: " + ex.Message);
            }
        }

        public List<SearchResult> Search(string filename)
        {
            List<SearchResult> results = new List<SearchResult>();

            foreach (KeyValuePair<Guid, MemberHandler> kvp in memberHandlers)
                results.AddRange(kvp.Value.Search(filename));

            return results;
        }

        public void RemoveMemberHandler(Guid memberHandlerId)
        {
            lock (memberHandlers)
            {
                memberHandlers.Remove(memberHandlerId);
            }
        }
    }
}
