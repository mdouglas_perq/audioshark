﻿/**
 * Audioshark Directory Protocol Library, v0.1
 * Copyright (c) 2009 Matthew Douglas, Jeff Sorbo
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 **/

using System;
using System.Collections.Generic;
using System.Text;

namespace Audioshark.AdpLibrary
{
    /// <summary>
    /// Enumeration of possible packet flags.
    /// </summary>
    [Flags]
    public enum AdpPacketFlags : byte
    {
        /// <summary>
        /// No flags set.
        /// </summary>
        None = 0x00,

        /// <summary>
        /// Reserved flag.
        /// </summary>
        Reserved1 = 0x01,

        /// <summary>
        /// Reserved flag.
        /// </summary>
        Reserved2 = 0x02,

        /// <summary>
        /// Reserved flag.
        /// </summary>
        Reserved3 = 0x04,

        /// <summary>
        /// The packet contains a data fragment.
        /// </summary>
        /// <remarks>This flag should be only set if Data is also set.</remarks>
        Fragment = 0x08,

        /// <summary>
        /// The packet contains application data.
        /// </summary>
        Data = 0x10,

        /// <summary>
        /// The packet is a FIN type.
        /// </summary>
        Fin = 0x20,

        /// <summary>
        /// The packet contains an acknowledgement field that is significant.
        /// </summary>
        Ack = 0x40,

        /// <summary>
        /// The packet is a SYN type.
        /// </summary>
        Syn = 0x80
    }

    /// <summary>
    /// Represents a packet of data in the Audioshark Directory Protocol.
    /// This protocol attempts to implement a reliable transfer on top of UDP.
    /// </summary>
    public class AdpPacket
    {
        private AdpPacketFlags flags = AdpPacketFlags.None;
        private byte length = 10;
        private UInt32 seqNumber = 0;
        private UInt32 ackNumber = 0;
        private byte[] data = null;

        /// <summary>
        /// Constructor.
        /// </summary>
        public AdpPacket()
        {  
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="f">Initial flags for the packet.</param>
        public AdpPacket(AdpPacketFlags f)
        {
            this.flags = f;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">Initial payload data for the packet.</param>
        public AdpPacket(byte[] data)
        {
            this.Data = data;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="seqNumber">The packet's sequence number.</param>
        /// <param name="ackNumber">The packet's acknowledgement number.</param>
        public AdpPacket(UInt32 seqNumber, UInt32 ackNumber)
        {
            this.SequenceNumber = seqNumber;
            this.AcknowledgementNumber = ackNumber;
        }

        /// <summary>
        /// 
        /// </summary>
        public UInt32 AcknowledgementNumber
        {
            get { return this.ackNumber; }
            set { this.ackNumber = value; }
        }

        /// <summary>
        /// Payload data carried by the packet.
        /// </summary>
        public byte[] Data
        {
            get { return this.data; }
            set {
                if (value == null)
                {
                    this.length = 10;
                    this.data = null;
                }
                else if (value.Length > 118)
                {
                    throw new FormatException("AdpPacket Data Too Large");
                }
                else
                {
                    this.data = value;
                    this.length = (byte)(10 + value.Length);
                }
            }
        }

        /// <summary>
        /// The packet's flags.
        /// </summary>
        public AdpPacketFlags Flags
        {
            get { return this.flags; }
            set { this.flags = value; }
        }

        /// <summary>
        /// The length of the packet, in bytes, including header.
        /// </summary>
        public byte Length
        {
            get { return this.length; }
        }

        /// <summary>
        /// The packet's sequence number.
        /// </summary>
        public UInt32 SequenceNumber
        {
            get { return this.seqNumber; }
            set { this.seqNumber = value; }
        }

        /// <summary>
        /// Constructs an AdpPacket object from a byte array.
        /// </summary>
        /// <param name="packet">The packet data</param>
        /// <returns>An AdpPacket object representing the packet.</returns>
        public static AdpPacket FromBytes(byte[] packet)
        {
            if (packet == null)
            {
                throw new ArgumentNullException();
            }
            else if (packet.Length < 10 || packet.Length > 128)
            {
                throw new FormatException("ADP Packet must be between 10 and 128 bytes long.");
            }
            else
            {
                AdpPacket adp = new AdpPacket();
                byte length;

                // Parse Header
                adp.Flags = (AdpPacketFlags)packet[0];
                length = packet[1];
                adp.SequenceNumber = BitConverter.ToUInt32(packet, 2);
                adp.AcknowledgementNumber = BitConverter.ToUInt32(packet, 6);

                if (packet.Length != length)
                {
                    throw new FormatException("ADP Packet Length Mismatch");
                }
                else
                {
                    if (length > 10)
                    {
                        byte[] data = new byte[length - 10];
                        Buffer.BlockCopy(packet, 10, data, 0, packet[2] - 10);
                        adp.Data = data;
                    }

                    return adp;
                }
            }
        }

        /// <summary>
        /// Get a byte representation of the packet.
        /// </summary>
        /// <returns>Raw packet data as an array of bytes.</returns>
        public byte[] GetBytes()
        {
            byte[] packet = new byte[this.length];
            packet[0] = (byte)this.flags;
            packet[1] = this.length;
            Buffer.BlockCopy(BitConverter.GetBytes(this.seqNumber), 0, packet, 2, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(this.ackNumber), 0, packet, 6, 4);

            if (this.data != null)
                Buffer.BlockCopy(this.data, 0, packet, 0, this.data.Length);

            return packet;
        }

        /// <summary>
        /// Checks for the existence of specified flags.
        /// </summary>
        /// <param name="f">The flags to check.</param>
        /// <returns>true if the packet contains the flags f, false otherwise.</returns>
        public bool HasFlags(AdpPacketFlags f)
        {
            return (this.flags & f) == f;
        }
    }
}
