﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AudiosharkDirectoryServer
{
    class AudiosharkMember
    {
        private string username;
        private System.Net.IPAddress ipAddress;

        public string Username
        {
            get { return username; }
            set { username = value; }
        }

        public System.Net.IPAddress IPAddress
        {
            get { return ipAddress; }
            set { ipAddress = value; }
        }

        public AudiosharkMember(string username, System.Net.IPAddress ipAddress)
        {
            this.username = username;
            this.ipAddress = ipAddress;
        }

        public AudiosharkMember() : this("", null) { }
    }
}
