﻿/**
 * Audioshark Servent, v0.1
 * GUI Component
 * Copyright (c) 2009 Matthew Douglas, Jeff Sorbo
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 **/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Audioshark.Servent
{
    public partial class AudiosharkServent : Form
    {
        private FileServer fileServer;
        private TextBoxWriter debugWriter;
        private DirectoryClient directory;

        public AudiosharkServent()
        {
            InitializeComponent();
        }

        private void AudiosharkServent_Load(object sender, EventArgs e)
        {
            // Capture Console output in debug TextBox
            debugWriter = new TextBoxWriter(this.txtDebug);
            Console.SetOut(debugWriter);

            Console.WriteLine(Application.ProductName + "/" + Application.ProductVersion +
                            " " + Environment.OSVersion.VersionString);
            Console.WriteLine("===========================================================");

            // Prevent flickering
            SetDoubleBuffered(lvUpload);
            SetDoubleBuffered(lvDownloads);
            SetDoubleBuffered(lvSearch);

            // Get shared path
            string path = Path.GetDirectoryName(Application.ExecutablePath) +
                          Path.DirectorySeparatorChar + "Shared";

            // Start file server in shared directory
            fileServer = new FileServer(path);
            fileServer.OnUploadBegin += new FileServer.FileServerUploadBeginEventHandler(fileServer_OnUploadBegin);
            fileServer.OnUploadComplete += new FileServer.FileServerEventHandler(fileServer_OnUploadComplete);
            fileServer.OnProgressUpdate += new FileServer.FileServerProgressHandler(fileServer_OnProgressUpdate);
            fileServer.OnError += new FileServer.FileServerErrorHandler(fileServer_OnError);
            fileServer.Start();

            // Watch the shared directory
            fileSystemWatcher.Path = fileServer.ShareDirectory;

            // Add existing shared files
            DirectoryInfo d = new DirectoryInfo(fileServer.ShareDirectory);
            foreach (FileInfo f in d.GetFiles("*.mp3", SearchOption.TopDirectoryOnly))
            {
                lvLibrary.Items.Add(f.Name);
            }

            // Create a directory client
            directory = new DirectoryClient();

            // Subscribe to directory events
            directory.OnConnect += new DirectoryClient.DirectoryClientEventHandler(directory_OnConnect);
            directory.OnDisconnect += new DirectoryClient.DirectoryClientEventHandler(directory_OnDisconnect);
            directory.OnResponseError += new DirectoryClient.DirectoryClientEventHandler(directory_OnResponseError);
            directory.OnResponseNotImplemented += new DirectoryClient.DirectoryClientEventHandler(directory_OnResponseNotImplemented);
            directory.OnResponseOK += new DirectoryClient.DirectoryClientEventHandler(directory_OnResponseOK);
            directory.OnSearchResult += new DirectoryClient.DirectoryClientSearchResultHandler(directory_OnSearchResult);
            directory.OnSocketError += new DirectoryClient.DirectoryClientSocketErrorHandler(directory_OnSocketError);

            // Connect to the directory server
            this.Connect();
        }

        private void AudiosharkServent_FormClosing(object sender, FormClosingEventArgs e)
        {
            fileServer.Stop();

            directory.SendRequest(DirectoryRequestMethod.Exit, null);

            Application.Exit();     // Todo: exit more cleanly.

            // Must check if file transfers are in progress.
            // Also properly shut down connection to the Directory server. 
            // Send an EXIT message.

            Environment.Exit(0);        // This is a hack...
        }

        private void Connect()
        {
            System.Net.IPAddress server = this.GetServerAddress();
            directory.Connect(server);
        }

        private System.Net.IPAddress GetServerAddress()
        {
            IPAddressDialog dlg = new IPAddressDialog();
            System.Net.IPAddress result = null;

            if (dlg.ShowDialog(this) == DialogResult.OK)
            {
                result = System.Net.IPAddress.Parse(dlg.txtInput.Text);
            }
            dlg.Dispose();

            return result;
        }

        #region Directory Events
        private void directory_OnSocketError(int nativeCode, string message)
        {
        }

        private void directory_OnSearchResult(string filename, string size, string ip, string hostname)
        {
            if (this.lvSearch.InvokeRequired)
                this.lvSearch.BeginInvoke(new MethodInvoker(() => this.directory_OnSearchResult(filename, size, ip, hostname)));
            else
            {
                ListViewItem item = lvSearch.Items.Add(Guid.NewGuid().ToString(), filename, 0);
                item.SubItems.Add(SizeToString(size));
                item.SubItems.Add(hostname);
                item.SubItems.Add(ip);
            }
        }

        private void directory_OnResponseOK()
        {
        }

        private void directory_OnResponseNotImplemented()
        {
        }

        private void directory_OnResponseError()
        {
        }

        private void directory_OnDisconnect()
        {
            this.statusConnection.Text = "Disconnected";
            this.connectToolStripMenuItem.Enabled = true;
        }

        void directory_OnConnect()
        {
            this.statusConnection.Text = "Connected";
            this.connectToolStripMenuItem.Enabled = false;

            // send an initial INFORM
            Dictionary<string, string> headers = new Dictionary<string, string>();

            DirectoryInfo d = new DirectoryInfo(this.fileServer.ShareDirectory);
            foreach (FileInfo f in d.GetFiles("*.mp3", SearchOption.TopDirectoryOnly))
                headers.Add(f.Name, f.Length.ToString());

            directory.SendRequest(DirectoryRequestMethod.Inform, headers);
        }
        #endregion

        #region Menu Bar
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(null, 
                            Application.ProductName + "/" +
                            Application.ProductVersion + " (" +
                            Environment.OSVersion.VersionString + ")\n" +
                            "by Matthew Douglas and Jeff Sorbo\n" +
                            "Central Connecticut State University\n" +
                            "CS 490 - Computer Networks, Fall 2009\n\n" + 
                            "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, " +
                            "EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES " +
                            "OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND " +
                            "NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT " +
                            "HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, " +
                            "WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING " +
                            "FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR " +
                            "OTHER DEALINGS IN THE SOFTWARE.", "About Audioshark",
                            MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        }

        private void connectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Connect();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void preferencesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This feature is not yet implemented.", "Not Implemented",
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }
        #endregion

        #region Filesystem Events
        private void fileSystemWatcher_Created(object sender, System.IO.FileSystemEventArgs e)
        {
            Console.WriteLine("Library: Object '{0}' created.", e.Name);

            // Send an inform message
            Dictionary<string, string> headers = new Dictionary<string, string>();
            FileInfo f = new FileInfo(e.FullPath);
            headers.Add(e.Name, f.Length.ToString());
            directory.SendRequest(DirectoryRequestMethod.Inform, headers);
            
        }

        private void fileSystemWatcher_Deleted(object sender, System.IO.FileSystemEventArgs e)
        {
            Console.WriteLine("Libary: Object '{0}' deleted.", e.Name);

            // Send a delete message
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add(e.Name, "");
            directory.SendRequest(DirectoryRequestMethod.Delete, headers);
        }

        private void fileSystemWatcher_Renamed(object sender, System.IO.RenamedEventArgs e)
        {
            Console.WriteLine("Library: Object '{0}' renamed to '{1}'", e.OldName, e.Name);

            // Send a delete message
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add(e.OldName, "");
            directory.SendRequest(DirectoryRequestMethod.Delete, headers);

            // Send an inform message
            headers = new Dictionary<string, string>();
            FileInfo f = new FileInfo(e.FullPath);
            headers.Add(e.Name, f.Length.ToString());
            directory.SendRequest(DirectoryRequestMethod.Inform, headers);
        }
        #endregion

        #region GUI Events
        private void btnSearch_Click(object sender, EventArgs e)
        {
            lvSearch.Items.Clear();
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Search", txtSearch.Text);
            this.directory.SendRequest(DirectoryRequestMethod.Query, headers);
        }

        private void lvSearch_DoubleClick(object sender, EventArgs e)
        {
            string path = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) +
                            System.IO.Path.DirectorySeparatorChar + "Downloads";

            string filename = lvSearch.SelectedItems[0].Text;
            string size = lvSearch.SelectedItems[0].SubItems[1].Text;
            string hostname = lvSearch.SelectedItems[0].SubItems[2].Text;
            string hostIP = lvSearch.SelectedItems[0].SubItems[3].Text;

            Console.WriteLine("GUI: Requesting download of '{0}' from '{1}'", filename, hostIP);

            FileRequest fileRequest = new FileRequest(path, hostIP, filename);
            fileRequest.OnConnect += new FileRequest.FileRequestEventHandler(fileRequest_OnConnect);
            fileRequest.OnDownloadBegin += new FileRequest.FileRequestEventHandler(fileRequest_OnDownloadBegin);
            fileRequest.OnDownloadComplete += new FileRequest.FileRequestEventHandler(fileRequest_OnDownloadComplete);
            fileRequest.OnError += new FileRequest.FileRequestErrorHandler(fileRequest_OnError);
            fileRequest.OnProgressUpdate += new FileRequest.FileRequestProgressHandler(fileRequest_OnProgressUpdate);

            AddDownloadEntry(fileRequest.Key, filename, size, hostIP, hostname);


            Thread t = new Thread(new ThreadStart(fileRequest.Download));
            t.Start();

            tabControl.SelectedTab = tabTransfers;

        }
        #endregion

        #region Download Events
        private void AddDownloadEntry(string key, string filename, string size, string hostIP, string hostname)
        {
            if (this.lvDownloads.InvokeRequired)
                this.lvDownloads.BeginInvoke(new MethodInvoker(() => this.AddDownloadEntry(key, filename, size, hostIP, hostname)));
            else
            {
                ListViewItem item = lvDownloads.Items.Add(key, filename, 0);
                item.SubItems.Add(SizeToString(size));
                item.SubItems.Add(hostname);
                item.SubItems.Add(hostIP);
                item.SubItems.Add("Connecting...");
            }
        }

        private void fileRequest_OnProgressUpdate(FileRequest src, long bytesRead, long bytesTotal)
        {
            if (this.lvDownloads.InvokeRequired)
                this.lvDownloads.BeginInvoke(new MethodInvoker(() => this.fileRequest_OnProgressUpdate(src, bytesRead, bytesTotal)));
            else if (lvDownloads.Items.ContainsKey(src.Key))
            {
                
                lvDownloads.Items[src.Key].SubItems[4].Text = SizeToString(bytesRead) + " / " + SizeToString(bytesTotal) +
                                                              String.Format(" ({0:#%})", bytesRead / bytesTotal); ;
            }
        }
        private void fileRequest_OnError(FileRequest src, string errorMessage)
        {
            if (this.lvDownloads.InvokeRequired)
                this.lvDownloads.BeginInvoke(new MethodInvoker(() => this.fileRequest_OnError(src, errorMessage)));
            else if (lvDownloads.Items.ContainsKey(src.Key))
            {
                lvDownloads.Items[src.Key].SubItems[4].Text = "Error: " + errorMessage;
            }
        }
        private void fileRequest_OnDownloadComplete(FileRequest src)
        {
            if (this.lvDownloads.InvokeRequired)
                this.lvDownloads.BeginInvoke(new MethodInvoker(() => this.fileRequest_OnDownloadComplete(src)));
            else if (lvDownloads.Items.ContainsKey(src.Key))
            {
                lvDownloads.Items[src.Key].SubItems[4].Text = "Completed";
            }
        }
        private void fileRequest_OnDownloadBegin(FileRequest src)
        {
        }
        private void fileRequest_OnConnect(FileRequest src)
        {
            if (this.lvDownloads.InvokeRequired)
                this.lvDownloads.BeginInvoke(new MethodInvoker(() => this.fileRequest_OnConnect(src)));
            else if (lvDownloads.Items.ContainsKey(src.Key))
            {
                lvDownloads.Items[src.Key].SubItems[4].Text = "Sending Request...";
            }
        }
        #endregion

        #region Upload Events
        private void AddUploadEntry(string key, string filename, string size, string hostIP, string hostname)
        {
            if (this.lvUpload.InvokeRequired)
                this.lvUpload.BeginInvoke(new MethodInvoker(() => this.AddUploadEntry(key, filename, size, hostIP, hostname)));
            else
            {
                ListViewItem item = lvUpload.Items.Add(key, filename, 0);
                item.SubItems.Add(SizeToString(size));
                item.SubItems.Add(hostname);
                item.SubItems.Add(hostIP);
                item.SubItems.Add("Initializing...");
            }
        }

        private void fileServer_OnError(string key, string errorMessage)
        {
            if (this.lvUpload.InvokeRequired)
                this.lvUpload.BeginInvoke(new MethodInvoker(() => this.fileServer_OnError(key, errorMessage)));
            else if (lvUpload.Items.ContainsKey(key))
            {
                lvUpload.Items[key].SubItems[4].Text = "Error: " + errorMessage;
            }
        }

        private void fileServer_OnProgressUpdate(string key, long bytesRead, long bytesTotal)
        {
            if (this.lvUpload.InvokeRequired)
                this.lvUpload.BeginInvoke(new MethodInvoker(() => this.fileServer_OnProgressUpdate(key, bytesRead, bytesTotal)));
            else if (lvUpload.Items.ContainsKey(key))
            {
                lvUpload.Items[key].SubItems[4].Text = SizeToString(bytesRead) + " / " + SizeToString(bytesTotal) +
                                                       String.Format(" ({0:#%})", bytesRead / bytesTotal);

            }
        }

        private void fileServer_OnUploadComplete(string key)
        {
            if (this.lvUpload.InvokeRequired)
                this.lvUpload.BeginInvoke(new MethodInvoker(() => this.fileServer_OnUploadComplete(key)));
            else if (lvUpload.Items.ContainsKey(key))
            {
                lvUpload.Items[key].SubItems[4].Text = "Completed";
            }
        }

        private void fileServer_OnUploadBegin(string key, string filename, string size, string hostname, string ip)
        {
            AddUploadEntry(key, filename, size, ip, hostname);
        }
        #endregion

        #region Utilities
        // from http://msdn.microsoft.com/en-us/library/system.windows.forms.listview.doublebuffered.aspx
        private static void SetDoubleBuffered(Control control)
        {
            // set instance non-public property with name "DoubleBuffered" to true
            typeof(Control).InvokeMember("DoubleBuffered",
            BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
            null, control, new object[] { true });
        }

        private static string SizeToString(string size)
        {
            try
            {
                return SizeToString(long.Parse(size));
            }
            catch (Exception)
            {
                return size;
            }
        }

        private static string SizeToString(long size)
        {
            string s;

            if (size >= (1024 * 1024 * 1024))
                s = String.Format("{0:##.##}", (double)size / (1024 * 1024 * 1024)) + " GB";
            else if (size >= (1024 * 1024))
                s = String.Format("{0:##.##}", (double)size / (1024 * 1024)) + " MB";
            else if (size >= 1024)
                s = String.Format("{0:##.##}", (double)size / 1024) + " KB";
            else
                s = size.ToString() + " Bytes";

            return s;
        }
        #endregion

    }
}
