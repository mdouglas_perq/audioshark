﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace AudiosharkDirectoryServer
{
    static class Program
    {
        static void Main()
        {
            Console.WriteLine(Application.ProductName + "/" + Application.ProductVersion +
                            " " + Environment.OSVersion.VersionString);
            Console.WriteLine("===========================================================");

            AudiosharkDirectoryServer server = new AudiosharkDirectoryServer();
            server.Start();

            Console.WriteLine("Server started...");
        }
    }
}
