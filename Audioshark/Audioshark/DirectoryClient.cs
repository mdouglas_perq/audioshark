﻿/**
 * Audioshark Servent, v0.1
 * Directory Client Component
 * Copyright (c) 2009 Matthew Douglas, Jeff Sorbo
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 **/

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;

namespace Audioshark.Servent
{
    public enum DirectoryRequestMethod
    {
        Inform,
        Delete,
        Query,
        Exit
    }

    public class DirectoryClient
    {
        // Delegates types for event handlers
        public delegate void DirectoryClientEventHandler();
        public delegate void DirectoryClientSocketErrorHandler(int nativeCode, string message);
        public delegate void DirectoryClientSearchResultHandler(string filename, string size, string ip, string hostname);

        // Events
        public event DirectoryClientEventHandler OnConnect;
        public event DirectoryClientSearchResultHandler OnSearchResult;
        public event DirectoryClientEventHandler OnDisconnect;
        public event DirectoryClientEventHandler OnResponseOK;
        public event DirectoryClientEventHandler OnResponseError;
        public event DirectoryClientEventHandler OnResponseNotImplemented;
        public event DirectoryClientSocketErrorHandler OnSocketError;


        private Socket client;
        private IPAddress serverIP;
        private const int SERVER_PORT = 4490;
        private byte[] recvBuffer = new byte[4096];

        private Thread connectionThread;

        public DirectoryClient()
        {
            this.client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }


        public void Connect(IPAddress server)
        {
            if (!this.client.Connected)
            {
                if (server == null)
                    throw new ArgumentNullException();

                this.serverIP = server;
                this.connectionThread = new Thread(new ThreadStart(this.HandleConnection));
                this.connectionThread.Start();
            }
        }

        public void SendRequest(DirectoryRequestMethod method, Dictionary<string, string> headers)
        {
            if (this.client.Connected)
            {
                string request = "";

                switch (method)
                {
                    case DirectoryRequestMethod.Delete: request += "DELETE"; break;
                    case DirectoryRequestMethod.Exit: request += "EXIT"; break;
                    case DirectoryRequestMethod.Inform: request += "INFORM"; break;
                    case DirectoryRequestMethod.Query: request += "QUERY"; break;
                    default: return;    // Invalid method
                }

                Console.WriteLine("DirectoryClient: Sending '{0}' Request", request);

                request += " " + Dns.GetHostName() + "\r\n";

                // Add headers
                if (headers != null)
                    foreach (KeyValuePair<string, string> kvp in headers)
                        request += kvp.Key + ": " + kvp.Value + "\r\n";

                request += "\r\n";

                this.client.Send(Encoding.ASCII.GetBytes(request));
            }
            else
            {
                Console.WriteLine("DirectoryClient: Cannot send request while disconnected");
            }
        }

        private void HandleConnection()
        {
            try
            {
                IPEndPoint ep = new IPEndPoint(this.serverIP, SERVER_PORT);
                this.client.Connect(ep);

                // raise an OnConnect event
                Console.WriteLine("DirectoryClient: Connected to server: {0}", ep);
                if (this.OnConnect != null)
                    OnConnect();

                NetworkStream ns = new NetworkStream(client);
                StreamReader stream = new StreamReader(ns);

                string line;
                while (client.Connected)
                {
                    
                    line = stream.ReadLine();

                    if (line != null)
                    {
                        if (line.IndexOf("200 OK") > 0)
                        {
                            // Raise OnResponseOK
                            Console.WriteLine("DirectoryClient: Received '200 OK'");
                            if (this.OnResponseOK != null)
                                this.OnResponseOK();
                        }
                        else if (line.IndexOf("400 ERROR") > 0)
                        {
                            // Raise OnResponseError
                            Console.WriteLine("DirectoryClient: Received '400 ERROR'");
                            if (this.OnResponseError != null)
                                this.OnResponseError();
                        }
                        else if (line.IndexOf("500 Internal Server Error") > 0)
                        {
                            // Raise OnResponseError
                            Console.WriteLine("DirectoryClient: Received '500 Internal Server Error'");
                            if (this.OnResponseError != null)
                                this.OnResponseError();
                        }
                        else if (line.IndexOf("501 Not Implemented") > 0)
                        {
                            // Raise OnResponseNotImplemented
                            Console.WriteLine("DirectoryClient: Received '501 Not Implemented'");
                            if (this.OnResponseNotImplemented != null)
                                this.OnResponseNotImplemented();
                        }
                        else if (line.IndexOf(':') > 0)
                        {
                            // check if the line has format FileName: size,IP,hostname

                            try
                            {
                                string[] parts = line.Split(new char[] { ':' }, 2);
                                string[] attributes = parts[1].Split(new char[] { ',' }, 3);

                                string name = parts[0].Trim();
                                string size = attributes[0].Trim();
                                string address = attributes[1].Trim();
                                string hostname = attributes[2].Trim();

                                // todo: nice size 

                                if (this.OnSearchResult != null)
                                    this.OnSearchResult(name, size, address, hostname);
                            }
                            catch (Exception)
                            {
                            }

                        }
                    }
                }

                stream.Close();
                ns.Close();
                client.Close();

                // Raise OnDisconnect
                Console.WriteLine("DirectoryClient: Disconnected");
                if (this.OnDisconnect != null)
                    this.OnDisconnect();

                return;
            }
            catch (SocketException ex)
            {
                // Raise OnSocketError
                Console.WriteLine("DirectoryClient: Socket Error #{0}: {1}", ex.NativeErrorCode, ex.Message);
                if (this.OnSocketError != null)
                    this.OnSocketError(ex.NativeErrorCode, ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("DirectoryClient: Exception: {0}", ex.Message);
            }

            Console.WriteLine("DirectoryClient: Disconnected");
            if (this.OnDisconnect != null)
                this.OnDisconnect();

            this.client.Close();
        }
    }
}
