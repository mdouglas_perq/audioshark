﻿/**
 * Audioshark Servent, v0.1
 * File Download Component
 * Copyright (c) 2009 Matthew Douglas, Jeff Sorbo
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 **/

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Audioshark.Servent
{
    /// <summary>
    /// Requests a file download.
    /// </summary>
    public class FileRequest
    {
        // Delegates types for event handlers
        public delegate void FileRequestEventHandler(FileRequest src);
        public delegate void FileRequestProgressHandler(FileRequest src, long bytesRead, long bytesTotal);
        public delegate void FileRequestErrorHandler(FileRequest src, string errorMessage);

        // Events
        public event FileRequestEventHandler OnConnect;
        public event FileRequestEventHandler OnDownloadBegin;
        public event FileRequestEventHandler OnDownloadComplete;
        public event FileRequestProgressHandler OnProgressUpdate;
        public event FileRequestErrorHandler OnError;
     
        private Socket client;
        private string hostIP;
        private int hostPort = 8490;
        private string filename;

        private string key;     // Unique key to identify this request

        private string saveDir = "\\Downloads";


        public string Key
        {
            get { return this.key; }
        }

        /// <summary>
        /// Returns the location of the save directory.
        /// </summary>
        public string SaveDirectory
        {
            get { return this.saveDir; }
        }

        /// <summary>
        /// Constructs a new FileRequest.
        /// </summary>
        /// <param name="savePath">The location where the file should be saved.</param>
        /// <param name="hostIP">The IP address of the file host.</param>
        /// <param name="filename">The name of the file to request.</param>
        public FileRequest(string savePath, string hostIP, string filename)
        {
            this.saveDir = savePath;
            this.hostIP = hostIP;
            this.filename = filename;

            this.key = Guid.NewGuid().ToString();

            if (!Directory.Exists(saveDir))
                Directory.CreateDirectory(saveDir);

            Console.WriteLine("FileRequest: Created. IP={0}, file='{1}'; SavePath='{2}'", 
                                this.hostIP, this.filename, Path.GetFullPath(this.saveDir));
        }

        /// <summary>
        /// Tests the FileRequest class.
        /// </summary>
        public static void Test()
        {
            string path = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) +
                          System.IO.Path.DirectorySeparatorChar + "Downloads";

            FileRequest testRequest = new FileRequest(path, "127.0.0.1", "XVicarious.mp3");
            testRequest.OnConnect += new FileRequestEventHandler(testRequest_OnConnect);
            testRequest.OnDownloadBegin += new FileRequestEventHandler(testRequest_OnDownloadBegin);
            testRequest.OnDownloadComplete += new FileRequestEventHandler(testRequest_OnDownloadComplete);
            testRequest.OnProgressUpdate += new FileRequestProgressHandler(testRequest_OnProgressUpdate);
            testRequest.OnError += new FileRequestErrorHandler(testRequest_OnError);

            Thread reqThread = new Thread(new ThreadStart(testRequest.Download));
            reqThread.Start();
        }

        static void testRequest_OnError(FileRequest src, string errorMessage)
        {
            //throw new NotImplementedException();
        }

        static void testRequest_OnProgressUpdate(FileRequest src, long bytesRead, long bytesTotal)
        {
            //throw new NotImplementedException();
        }

        static void testRequest_OnDownloadComplete(FileRequest src)
        {
            //throw new NotImplementedException();
        }

        static void testRequest_OnDownloadBegin(FileRequest src)
        {
            //throw new NotImplementedException();
        }

        static void testRequest_OnConnect(FileRequest src)
        {
            //throw new NotImplementedException();
        }

        /// <summary>
        /// Send the request and attempt to download the file.
        /// </summary>
        /// <remarks>This is a blocking function and should run in its own thread.</remarks>
        public void Download()
        {
            // Create a TCP socket
            this.client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                // Connect to the remote host
                this.client.Connect(IPAddress.Parse(this.hostIP), this.hostPort);

                // Notify event subscribers that connection has been established
                Console.WriteLine("FileRequest: Connected to Remote Host: {0}", this.client.RemoteEndPoint.ToString());
                if (OnConnect != null)
                    OnConnect(this);

                // TODO: UriEncode filename before sending

                // Compose the request
                string request = "GET /" + filename + " HTTP/1.1\r\n" +
                                 "Host: " + this.hostIP + "\r\n" +
                                 "User-Agent: " + System.Windows.Forms.Application.ProductName + "/" +
                                                  System.Windows.Forms.Application.ProductVersion + " (" +
                                                  Environment.OSVersion.VersionString + ")\r\n" +
                                 "Connection: close\r\n" +
                                 "\r\n";

                // Send request
                client.Send(Encoding.ASCII.GetBytes(request));

                NetworkStream ns = new NetworkStream(client);
                StreamReader stream = new StreamReader(ns);

                // Read the first line of the response
                string line = stream.ReadLine();

                // We should have HTTP response + status phrase
                string[] status = line.Split(new char[] { ' ' }, 3);
                string httpVersion = status[0];
                string httpCode = status[1];
                string httpPhrase = status[2];

                if (httpCode.Equals("200")) // HTTP 200 OK
                {
                    // Continue reading additional headers
                    Dictionary<string, string> headers = new Dictionary<string, string>();
                    while ((line = stream.ReadLine()) != "" && line != null)
                    {
                        string[] parts = line.Split(new char[] { ':', ' ' }, 2);
                        headers.Add(parts[0], parts[1]);
                    }

                    // Stream should be pointing at first byte of data...
                    int length = int.Parse(headers["Content-Length"]);
                    int bytesRead = 0;
                    int bytesRemaining = length;

                    string filePath = this.SaveDirectory + Path.DirectorySeparatorChar + filename;
                    filePath = filePath.Replace("..", "");
                    filePath = filePath.Replace('/', Path.DirectorySeparatorChar);
                    
                    // File stream for saving the downloaded file
                    //FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.Write);

                    string tempPath = Path.GetRandomFileName();
                    FileStream fs = new FileStream(tempPath, FileMode.Create, FileAccess.Write);
                    
                    Console.WriteLine("FileRequest: Beginning download of '{0}'", this.filename);
                    if (OnDownloadBegin != null)
                        OnDownloadBegin(this);

                    // Begin reading data
                    while (bytesRemaining > 0)
                    {
                        byte[] buffer = new byte[4096];

                        // Read data
                        int n = ns.Read(buffer, 0, buffer.Length);

                        if (n == 0)
                            break;      // EOF, or Error

                        // Write to file
                        fs.Write(buffer, 0, n);

                        // Dispatch progress update events
                        if (OnProgressUpdate != null)
                            OnProgressUpdate(this, bytesRead, length);

                        bytesRead += n;
                        bytesRemaining -= n;
                    }

                    // Close streams
                    fs.Close();
                    stream.Close();
                    ns.Close();

                    // Copy downloaded file
                    File.Move(tempPath, filePath);

                    // Dispatch download completion event
                    Console.WriteLine("FileRequest: Completed download of '{0}'", this.filename);
                    if (OnDownloadComplete != null)
                        OnDownloadComplete(this);
                }
                else
                {
                    // Received an HTTP response other than 200.
                    // Raise an error event
                    Console.WriteLine("FileRequest: Error requesting '{0}'; Response='{1} {2}'", this.filename, httpCode, httpPhrase);
                    if (OnError != null)
                        OnError(this, httpCode + " " + httpPhrase);
                }
            }
            catch (SocketException ex)
            {
                Console.WriteLine("FileRequest: Error requesting '{0}'; Socket Error #{1}: {2}",
                                        this.filename, ex.NativeErrorCode.ToString(), ex.Message);
                if (OnError != null)
                    OnError(this, "Socket Error #" + ex.NativeErrorCode.ToString());
            }
            // TODO: More detailed exception handling/error reporting
            catch (Exception ex)
            {
                Console.WriteLine("FileRequest: General Error requesting '{0}'; {1}", this.filename, ex.Message);

                if (OnError != null)
                    OnError(this, ex.Message);
            }
            finally
            {
                client.Close();
            }

            GC.Collect();
        }
    }
}
