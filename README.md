A P2P MP3 file sharing system developed as a final project in an undergraduate computer networking course. At the moment, this project is purely an academic exercise and is not intended for real-world use. 

Reference documentation: http://www.cs.ccsu.edu/~stan/classes/CS490/CS490-FA09-project.html