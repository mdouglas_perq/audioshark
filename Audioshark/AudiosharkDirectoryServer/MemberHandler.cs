﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace AudiosharkDirectoryServer
{
    class MemberHandler
    {
        private AudiosharkDirectoryServer server;
        private Socket client;

        private Guid memberHandlerId;
        private string hostname;
        private string ipAddress;
        private Dictionary<string, DirectoryEntry> entries;

        private const int BUFSIZE = 1024 * 1024;
        private const int TIMEOUT = 1;

        private const string INFORM_TAG = "INFORM",
            DELETE_TAG = "DELETE",
            QUERY_TAG = "QUERY",
            EXIT_TAG = "EXIT";

        private const string STATUS_OK = "200 OK",
                             STATUS_BADREQUEST = "400 Bad Request";

        private string[] LINE_SEPARATORS = { "\r\n" };

        public Dictionary<string, DirectoryEntry> Entries
        {
            get { return entries; }
            set { entries = value; }
        }

        public MemberHandler(Guid memberHandlerId, AudiosharkDirectoryServer server, Socket client)
        {
            this.memberHandlerId = memberHandlerId;
            this.server = server;
            this.client = client;
            this.ipAddress = ((IPEndPoint)client.RemoteEndPoint).Address.ToString();
            this.entries = new Dictionary<string, DirectoryEntry>();
        }

        public void HandleRequests()
        {
            bool alive = true;

            while (alive)
            {
                Console.WriteLine("Waiting for message from host " + hostname);
                byte[] reqBuffer = new byte[BUFSIZE];
                client.Receive(reqBuffer);
                string request = Encoding.ASCII.GetString(reqBuffer);

                // parse only up to the first blank line
                request = request.Substring(0, request.IndexOf("\r\n\r\n"));

                if (!request.StartsWith(INFORM_TAG) &&
                    !request.StartsWith(DELETE_TAG) &&
                    !request.StartsWith(QUERY_TAG) &&
                    !request.StartsWith(EXIT_TAG))
                {
                    HandleBadRequest();
                }

                else
                    alive = ParseMessage(request);
            }
            CloseClient();
        }

        private bool ParseMessage(string message)
        {
            string[] lines = message.Split(LINE_SEPARATORS, StringSplitOptions.RemoveEmptyEntries);

            // get the host name and ip from the first line
            ParseHostInfo(lines[0]);

            if (lines[0].StartsWith(INFORM_TAG))
                return ParseInform(lines);

            else if (lines[0].StartsWith(DELETE_TAG))
                return ParseDelete(lines);

            else if (lines[0].StartsWith(QUERY_TAG))
                return ParseQuery(lines);

            else if (lines[0].StartsWith(EXIT_TAG))
                return ParseExit();

            else // this should never happen
                return false;
        }

        private void ParseHostInfo(string line)
        {
            string[] hostInfo = line.Split(' ');
            hostname = hostInfo[1];
            Console.WriteLine("Hostname is " + hostname);
        }

        private bool ParseInform(string[] lines)
        {
            Console.WriteLine("INFORM message received from host " + hostname);
            // for each remaining line, get the file name and size, then add a directory entry
            for (int i = 1; i < lines.Count<string>(); i++)
            {
                string filename = lines[i].Substring(0, lines[i].IndexOf(':'));
                int size = int.Parse(lines[i].Substring(filename.Length + 1));
                AddEntry(new DirectoryEntry(filename, size));
            }
            return true;
        }

        private bool ParseDelete(string[] lines)
        {
            Console.WriteLine("DELETE message received from host " + hostname);
            // this will handle multiple file deletions in a single message
            for (int i = 1; i < lines.Count<string>(); i++)
            {
                string filename = lines[i].Substring(0, lines[i].IndexOf(':'));
                RemoveEntry(filename);
            }
            return true;
        }

        private bool ParseQuery(string[] lines)
        {
            Console.WriteLine("QUERY message received from host " + hostname);
            
            // assuming only one query string in message
            string searchString = lines[1];
            searchString = searchString.Replace("Search: ", "");
            string outputString = searchString.Length == 0 ? "Generating full directory listing..." : "Searching for file " + searchString;
            Console.WriteLine(outputString);

            List<SearchResult> results = server.Search(searchString);

            // create the response containing search results
            string response = STATUS_OK + "\r";

            foreach (SearchResult result in results)
            {
                string resultLine = result.Entry.Filename + ":" +
                    result.Entry.Size + "," +
                    result.IPAddress + "," +
                    result.Hostname + "\r";

                response += resultLine;
            }
            response += "\r";

            // send the response
            client.Send(Encoding.ASCII.GetBytes(response));

            return true;
        }

        private bool ParseExit()
        {
            Console.WriteLine("EXIT message received from host " + hostname);
            return false;
        }

        private void HandleBadRequest()
        {
            string response = STATUS_BADREQUEST + "\r\r";
            client.Send(Encoding.ASCII.GetBytes(response));
            Console.WriteLine("Bad request received from " + ipAddress);
        }

        private void CloseClient()
        {
            Console.WriteLine("Closing connection with host " + hostname);
            client.Close(TIMEOUT);
            server.RemoveMemberHandler(memberHandlerId);
            GC.Collect();
        }

        public System.Collections.Generic.List<SearchResult> Search(string filename)
        {
            System.Collections.Generic.List<SearchResult> listings = new List<SearchResult>();

            if (filename.Length > 0)
            {
                DirectoryEntry entry = new DirectoryEntry();

                if (entries.ContainsKey(filename))
                    entries.TryGetValue(filename, out entry);

                listings.Add(new SearchResult(ipAddress, hostname, entry));
            }
            else
            {
                foreach (KeyValuePair<string, DirectoryEntry> kvp in entries)
                    listings.Add(new SearchResult(ipAddress, hostname, kvp.Value));
            }

            return listings;
        }

        public void AddEntry(DirectoryEntry entry)
        {
            entries.Add(entry.Filename, entry);
            Console.WriteLine("Entry added for host {0}: {1}, {2} bytes.", hostname, entry.Filename, entry.Size);
        }

        public void RemoveEntry(string filename)
        {
            entries.Remove(filename);
            Console.WriteLine("File {0} removed from host {1}.", filename, hostname);
        }
    }
}
